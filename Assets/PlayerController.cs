﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.VR;

public class PlayerController : MonoBehaviour
{
    public int targetTypesCount;
    public Text premiosCounter;
    public Text premio;
    public Button[] targetTypesIcons;
    public UnityEvent OnHittedAllTargetTypes;

    public Canvas premiosCanvas;
    public Canvas counterCanvas;
    public Canvas outroCanvas;

    public TimerController timer;

    public int TargetsHit { get { return _targetsHit; } }
    int _targetsHit = 0;

    bool isTracking = true;
    bool[] targetTypes;
    IniFile.IniFile iniFile;

    Animator _animator;

    // Use this for initialization
    void Start ()
    {
        targetTypes = new bool[targetTypesCount];
        _animator = GetComponent<Animator>();
        iniFile = new IniFile.IniFile(Application.dataPath + "/settings.ini");
        Screen.fullScreen = true;
        timer.startSeconds = iniFile.GetInteger("Configuracion", "Tiempo", 60);
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (!isTracking)
        {
            Camera.main.transform.parent.rotation = Quaternion.Inverse(InputTracking.GetLocalRotation(VRNode.CenterEye));
        }
    }

    public void Reset()
    {
        _targetsHit = 0;
        for (int i = 0; i < targetTypes.Length; ++i)
        {
            targetTypes[i] = false;
        }
        for (int i = 0; i < targetTypesIcons.Length; ++i)
        {
            targetTypesIcons[i].interactable = true;
        }
    }

    public void SetPrize()
    {
        Debug.Log("PlayerController::SetPrize");
        foreach(string section in iniFile.GetAllSections())
        {
            if (iniFile.GetInteger(section, "Min") <= _targetsHit && _targetsHit <= iniFile.GetInteger(section, "Max"))
            {
                premiosCounter.text = _targetsHit.ToString();
                premio.text = string.Format("\nGanaste " + section);
                return;
            }
        }
    }

    public void HitTarget(int type)
    {
        if (!targetTypes[type])
        {
            targetTypes[type] = true;
            _targetsHit++;
            targetTypesIcons[type].interactable = false;
            Debug.Log("PlayerController::HitTarget - type: " + type);

            if (!(TargetsHit < targetTypes.Length))
            {
                Debug.Log("PlayerController::HitTarget - Hitted all targets!");
                OnHittedAllTargetTypes.Invoke();
            }
        }
    }

    public void EnableTracking()
    {
        Camera.main.transform.parent.rotation = Quaternion.identity;
        InputTracking.disablePositionalTracking = false;
        isTracking = true;
    }

    public void DisableTracking()
    {
        InputTracking.disablePositionalTracking = true;
        isTracking = false;
    }

    public void ShowPremios()
    {
        premiosCanvas.gameObject.SetActive(true);
    }

    public void HidePremios()
    {
        premiosCanvas.gameObject.SetActive(false);
    }

    public void ShowCounter()
    {
        counterCanvas.gameObject.SetActive(true);
    }

    public void HideCounter()
    {
        counterCanvas.gameObject.SetActive(false);
    }

    public void ShowOutro()
    {
        outroCanvas.gameObject.SetActive(true);
    }

    public void HideOutro()
    {
        outroCanvas.gameObject.SetActive(false);
    }
}
