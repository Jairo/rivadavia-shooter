﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Text))]
public class TimerController : MonoBehaviour
{
    public bool secondsOnly;
    public bool isPaused;
    public int startSeconds;
    public UnityEvent onComplete;

    double initialTime = 0f;
    Text text;

    public bool IsPaused
    {
        get { return isPaused; }
        set { isPaused = value; }
    }

    // Use this for initialization
    void Start()
    {
        Reset();
    }

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            double currentTime = Time.time;
            int delta = (startSeconds - ((int)(currentTime - initialTime)));
            text.text = (secondsOnly) ? delta.ToString() : DisplayTime(delta);

            if (delta < 1)
            {
                onComplete.Invoke();
                isPaused = true;
            }
        }
        else
        {
            initialTime += Time.deltaTime;
        }
    }

	private void OnDisable()
	{
        isPaused = true;
	}

	private void OnEnable()
	{
        Reset();
        isPaused = false;
    }

	public void Reset()
    {
        initialTime = Time.time;
        DisplayTime(startSeconds);
    }

    private string DisplayTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        return minutes.ToString("0") + ":" + seconds.ToString("00");
    }
}
