﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MyIntEvent : UnityEvent<int>
{
}

public class BoxController : MonoBehaviour
{
    //public float frequency = 20.0f;  // Speed of sine movement
    //public float magnitude = 0.5f;   // Size of sine movement

    public float floatingStrength = 10f;
    public float floatingFrequency = 120f;
    public int type;
    public Material otherMat;

    public MyIntEvent OnHit;

    Rigidbody _rigidbody;
    MeshRenderer _meshRenderer;
    Material normalMat;

    Coroutine ApplyForce;

    // Use this for initialization
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        GetComponent<Animator>().speed = Random.Range(.2f, .4f);
        ApplyForce = StartCoroutine(ApplyUpForceCoroutine(Random.Range(-1f, 1f)));
        _meshRenderer = GetComponent<MeshRenderer>();
        normalMat = _meshRenderer.material;
    }

    IEnumerator ApplyUpForceCoroutine(float random)
    {
        yield return new WaitForSeconds(random * floatingFrequency);
        _rigidbody.AddForce(new Vector3(random, Mathf.Abs(random), random) * floatingStrength * Time.deltaTime);
        ApplyForce = StartCoroutine(ApplyUpForceCoroutine(Random.Range(-1f, 1f)));
    }

    IEnumerator MoveToTheCenter()
    {
        float initialTime = Time.time;
        while (Time.time - initialTime < 2f)
        {
            yield return new WaitForFixedUpdate();
            _rigidbody.MovePosition(Vector3.Lerp(transform.position, new Vector3(.05f, 3.2f, 4.75f), Time.deltaTime));
        }

        ApplyForce = StartCoroutine(ApplyUpForceCoroutine(Random.Range(-1f, 1f)));
    }

    // Update is called once per frame
    void Update()
    {

        if (Mathf.Abs(transform.position.x) >= 70f ||
            Mathf.Abs(transform.position.y) >= 70f ||
            Mathf.Abs(transform.position.z) >= 70f)
        {
            _rigidbody.MovePosition(new Vector3(2f, 5f, 8f));
        }
        //Vector3 movement = transform.position + transform.right * Mathf.Sin(Time.time * frequency * Mathf.PI) * magnitude;
        //_rigidbody.MovePosition(movement + transform.up * Mathf.Sin(Time.time * frequency) * magnitude);
    }

    public void Hit()
    {
        StopCoroutine(ApplyForce);
        StartCoroutine(MoveToTheCenter());
        OnHit.Invoke(type);
    }

    void ChangeMaterial()
    {
        _meshRenderer.material = otherMat;
    }

    void RestoreMaterial()
    {
        _meshRenderer.material = normalMat;
    }
}
