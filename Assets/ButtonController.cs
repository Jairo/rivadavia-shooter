﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class ButtonController : MonoBehaviour
{
    public Texture idleTexture, pressedTexture;
    public SteamVR_Action_Boolean selectAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("InteractUI");
    public Hand hand;
    public UnityEvent OnClick;

    Material _material;

    public void _OnClick()
    {
        _material.SetTexture("_MainTex", pressedTexture);
        OnClick.Invoke();
    }

	// Use this for initialization
	void Start ()
    {
        _material = GetComponent<MeshRenderer>().material;
        _material.SetTexture("_MainTex", idleTexture);
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (selectAction.GetStateUp(hand.handType))
            _material.SetTexture("_MainTex", idleTexture);
    }
}
